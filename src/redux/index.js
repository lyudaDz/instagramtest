import { createStore, compose } from 'redux'

import rootReducer from './reducers'

const configureStore = () => {
  const enhancer = compose(
    window.devToolsExtension ? window.devToolsExtension() : f => f,
  )

  const store = createStore(rootReducer, enhancer)

  return store
}

export const store = configureStore()
