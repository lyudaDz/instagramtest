import { createReducer } from 'app/src/helpers/redux'

const initialState = {}

const handlers = {
}

export default createReducer(initialState, handlers)
