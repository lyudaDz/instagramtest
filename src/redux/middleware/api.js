// import normalize from 'json-api-normalizer'
import qs from 'qs'
import { AsyncStorage } from 'react-native'

import assign from 'lodash/assign'
import get from 'lodash/get'
import merge from 'lodash/merge'
import pick from 'lodash/pick'
import isEmpty from 'lodash/isEmpty'
import { Subject } from 'rxjs/Subject'
import request from 'superagent'

import { api } from 'app/src/config/App'

import { API_CALL } from 'app/src/constants/ids'

import { logOut } from 'app/Redux/actions/auth'

/* eslint-disable */
const sendMethod = HTTPMethod =>
  HTTPMethod === 'post' || HTTPMethod === 'put' || HTTPMethod === 'patch'
    ? 'send'
    : 'query'

const sendArguments = (HTTPMethod, query) =>
  HTTPMethod === 'post' || HTTPMethod === 'put' || HTTPMethod === 'patch'
    ? JSON.stringify(query)
    : qs.stringify(query, { arrayFormat: 'brackets' })

/*eslint-enable*/

const apiCall = (
  url = api.url,
  endpoint = '',
  method = 'GET',
  query = {},
  headers = {},
) => {
  const subject = new Subject()
  const HTTPMethod = method.toLowerCase()

  request[HTTPMethod](url + endpoint)
    [sendMethod(HTTPMethod)](sendArguments(HTTPMethod, query))
    .set(headers)
    .withCredentials()
    .end((error, data) => {
      if (isEmpty(data) || data.body === null) {
        merge(data, { body: { data: [] } })
      }

      if (error) {
        subject.error({ data, error })
      } else {
        subject.next({ rawData: data, method: HTTPMethod })
        subject.complete()
      }
    })

  return subject
}

const nextAction = (action, data) => {
  const next = merge({}, action, data)
  delete next[API_CALL]
  return next
}

export default store => next => action => {
  if (action.type !== API_CALL || !action.fields) return next(action)

  const {
    url,
    endpoint,
    headers,
    method,
    query,
    types,
    isRaw,
    callback = () => {},
  } = action.fields

  const signature = Date.now()
  // const authorizationHeader = getAuthorizationHeader(store.getState())
  const completeHeaders = assign(
    { 'Content-Type': 'application/vnd.api+json' },
    // authorizationHeader && (!url || url === api.url)
    //   ? { Authorization: authorizationHeader }
    //   : {},
    headers,
  )
  const fsaFields = pick(action.fields, 'payload', 'error', 'meta')
  const isLoadRequest =
    !method ||
    method.toUpperCase() === 'GET' ||
    method.toUpperCase() === 'PATCH' ||
    method.toUpperCase() === 'POST'

  next(
    nextAction(fsaFields, {
      type: types.REQUEST,
      meta: merge(
        { signature },
        isLoadRequest && { endpoint, isRequest: true },
      ),
    }),
  )
  const subject = new Subject()
  const apiRequest = apiCall(url, endpoint, method, query, completeHeaders)

  const onError = rawData => {
    const payload = get(rawData, 'data.body') || {}
    callback('Error')
    const data = {
      payload,
      type: types.FAILURE,
      meta: {
        signature,
        httpCode: rawData.error.status,
        isNetworkFailure: !rawData.error.status,
      },
      error: true,
    }

    // if (rawData.error.status === 401) {
    //   store.dispatch(logOut())
    // }

    next(nextAction(fsaFields, data))

    subject.error({
      httpCode: rawData.error.status,
      isNetworkFailure: !rawData.error.status,
      ...payload,
    })
  }

  const onSuccess = successData => {
    const { rawData } = successData

    if (rawData.body.status && rawData.body.status === 'Failure') {
      onError(rawData)
      return
    }

    // const normalized = isRaw
    //   ? get(rawData, 'body')
    //   : normalize(get(rawData, 'body'), {
    //       endpoint,
    //       camelizeKeys: true
    //     })
    const normalized = rawData.body

    const meta = merge({ signature }, { endpoint, isSuccess: true })

    const deletedId =
      successData.method.toUpperCase() === 'DELETE'
        ? endpoint.substring(endpoint.lastIndexOf('/') + 1, endpoint.length)
        : null

    let payload = { data: normalized }
    payload = deletedId !== null ? { ...payload, deletedId } : payload

    const data = { meta, payload, type: types.SUCCESS, isRaw }

    if (action.fields.meta && action.fields.meta.remember) {
      try {
        AsyncStorage.setItem('userId', normalized['user_id'])
      } catch (error) {
      }
    }

    next(nextAction(fsaFields, data))
    callback('Success')
    subject.next(payload)
    subject.complete()
  }

  apiRequest.subscribe(onSuccess, onError)

  return subject
}
