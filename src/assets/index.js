import Main from "./navigation/Main.png";
import AddPhoto from "./navigation/AddPhoto.png";
import Favourite from "./navigation/Favourite.png";
import Search from "./navigation/Search.png";
import User from "./navigation/User.png";
import MainActive from "./navigation/MainActive.png";
import UserActive from "./navigation/UserActive.png";
import SearchActive from "./navigation/SearchActive.png";
import FavouriteActive from "./navigation/FavouriteActive.png";
import First from "./usersPhotos/1.jpeg";
import Second from "./usersPhotos/2.jpeg";
import Third from "./usersPhotos/3.jpeg";
import Fourth from "./usersPhotos/4.jpeg";
import Fifth from "./usersPhotos/5.jpeg";
import Sixth from "./usersPhotos/6.jpeg";
import Seventh from "./usersPhotos/7.jpeg";
import Bubble from "./symbolIcons/Bubble.png";
import Direct from "./symbolIcons/Direct.png";
import Fav from "./symbolIcons/Fav.png";
import FavActive from "./symbolIcons/FavActive.png";

export {
  Main,
  AddPhoto,
  Favourite,
  Search,
  User,
  MainActive,
  UserActive,
  SearchActive, 
  FavouriteActive,
  First,
  Second,
  Third,
  Fourth,
  Fifth,
  Sixth,
  Seventh,
  Bubble,
  Direct,
  Fav,
  FavActive
};
