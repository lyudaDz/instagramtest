import { createBottomTabNavigator, createAppContainer } from 'react-navigation'
import { Footer } from 'app/src/components/Blocks'

import MainScreen from 'app/src/components/Screens/Main'
import SearchScreen from 'app/src/components/Screens/Search'
import AddPhotoScreen from 'app/src/components/Screens/AddPhoto'
import FavouriteScreen from 'app/src/components/Screens/Favourite'
import UserScreen from 'app/src/components/Screens/User'

const AppNavigator = createBottomTabNavigator(
  {
    Main: MainScreen,
    Search: SearchScreen,
    AddPhoto: AddPhotoScreen,
    Favourite: FavouriteScreen,
    User: UserScreen
  },
  {
    tabBarComponent: Footer,
    initialRouteName: 'Main',
    tabBarPosition: 'top',
  },
)

export default createAppContainer(AppNavigator)
