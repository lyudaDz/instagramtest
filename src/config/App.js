export const APP_ENV = process.env.NODE_ENV

export const api = {
  url:
    APP_ENV === `production`
      ? `https://dev.api.wupwoo.com`
      : `http://192.168.0.109:8080`,
}
