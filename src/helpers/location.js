import { PermissionsAndroid, Platform } from 'react-native'
import Geolocation from 'react-native-geolocation-service'
import AsyncStorage from '@react-native-community/async-storage'
import { isFunction } from 'lodash'

import { mapsApi } from 'app/src/constants/mapsApi.js'

let Loaded = false
let country = ''

export const distance = (lat1, lon1, lat2, lon2) => {
  if (lat1 == lat2 && lon1 == lon2) {
    return 0
  } else {
    var radlat1 = (Math.PI * lat1) / 180
    var radlat2 = (Math.PI * lat2) / 180
    var theta = lon1 - lon2
    var radtheta = (Math.PI * theta) / 180
    var dist =
      Math.sin(radlat1) * Math.sin(radlat2) +
      Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
    if (dist > 1) {
      dist = 1
    }
    dist = Math.acos(dist)
    dist = (dist * 180) / Math.PI
    dist = dist * 60 * 1.1515
    dist = dist * 1.609344 * 1000
    return dist
  }
}

export const hasLocationPermission = async () => {
  if (
    Platform.OS === 'ios' ||
    (Platform.OS === 'android' && Platform.Version < 23)
  ) {
    return true
  }

  const hasPermission = await PermissionsAndroid.check(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  )

  if (hasPermission) return true

  const status = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  )

  if (status === PermissionsAndroid.RESULTS.GRANTED) return true

  if (status === PermissionsAndroid.RESULTS.DENIED) {
    ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG)
  } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
    ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG)
  }

  return false
}

export const getLocation = async (
  onSetLocation,
  deviceCoordinates,
  callback,
) => {
  const hasPermission = await hasLocationPermission()

  if (!hasPermission) return
  Geolocation.getCurrentPosition(
    position => {
      if (!Loaded) {
        let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&key=${mapsApi.mapsKey}&sensor=true`
        fetch(url)
          .then(response => response.json())
          .then(json => {
            country = json.results[
              json.results.length - 1
            ].address_components[0].short_name.toLowerCase()
            const location = {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
            }

            if (onSetLocation) {
              onSetLocation({ ...location }, country)
              AsyncStorage.setItem('location', JSON.stringify(location))
            }
            if (isFunction(callback)) {
              callback(location)
            }
          })
        Loaded = true
      } else {

        let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude},${position.coords.longitude}&key=${mapsApi.mapsKey}&sensor=true`
        fetch(url)
          .then(response => response.json())
          .then(json => {
            country = json.results[
              json.results.length - 1
            ].address_components[0].short_name.toLowerCase()
            const location = {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
            }

            if (onSetLocation) {
              onSetLocation({ ...location }, country)
              AsyncStorage.setItem('location', JSON.stringify(location))
            }
            if (isFunction(callback)) {
              callback(location)
            }
          })

        // const location = {
        //   latitude: position.coords.latitude,
        //   longitude: position.coords.longitude,
        // }
        // if (onSetLocation) {
        //   onSetLocation({ ...location }, 'ua')
        //   AsyncStorage.setItem('location', JSON.stringify(location))
        // }
        // if (callback) {
        //   callback(location)
        // }
      }
    },
    async error => {
      if (isEmpty(deviceCoordinates)) {
        const location = await AsyncStorage.getItem('location')
        if (location) {
          onSetLocation({ ...JSON.parse(location) })
          if (callback) {
            callback(location)
          }
        }
      } else {
        if (callback) {
          callback(deviceCoordinates)
        }
      }
    },
    {
      enableHighAccuracy: true,
      timeout: 15000,
      maximumAge: 10000,
      distanceFilter: 50,
    },
  )
}
