import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import { User } from './User.js'


const selectors = createStructuredSelector({
})

const actions = {
}

export default connect(
  selectors,
  actions,
)(User)
