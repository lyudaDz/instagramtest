import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import { Search } from './Search.js'

const actions = {

}

const selectors = createStructuredSelector({
})

export default connect(
  selectors,
  actions,
)(Search)
