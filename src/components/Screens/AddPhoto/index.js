import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import AddPhoto from './AddPhoto.js'

const selectors = createStructuredSelector({

})

const actions = {

}

export default connect(
  selectors,
  actions,
)(AddPhoto)
