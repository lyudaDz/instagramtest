import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import { Favourite } from './Favourite.js'

const selectors = createStructuredSelector({})

const actions = {}

export default connect(
  selectors,
  actions,
)(Favourite)
