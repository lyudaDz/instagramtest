import styled from 'styled-components'
import Overlay from 'react-native-modal-overlay'
import { StyledView } from 'app/src/components/UI/'

export const Container = styled.View`
  flex: 1;
  background: white;
`

export const ScrollPosts = styled.ScrollView`
  width: 100%;
  height: 100%;
`

export const FlatListPosts = styled.FlatList`
  width: 100%;
`

export const Card = styled.View`
  width: 100%;
`

export const CardHeader = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 45;
`

export const CardHeaderButton = styled.TouchableOpacity`
  width: 15;
  justify-content: space-between;
  align-items: center;
  margin-right: 10;
`

export const CardHeaderButtonCicrcle = styled.View`
  width: 3;
  height: 3;
  background: black;
  border-radius: 50;
  margin: 2px;
`

export const CardImage = styled.Image`
  width: 100%;
  height: 750;
  resizeMode: cover;
`

export const UserAvatarWrapper = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  height: 40;
  margin-left: 15;
  overflow: hidden;
`

export const UserAvatarText = styled.Text`
  font-weight: bold;
  color: black;
`

export const HeaderImageWrapper = styled.View`
  width: 35;
  height: 35;
  margin-right: 10;
  padding: 5px;
  justify-content: space-around;
  align-items: center;
  border-radius: 50;
  overflow: hidden;
`

export const HeaderImage = styled.Image`
  width: 35;
  height: 35;
  margin-bottom: 1;
`

export const CardFooter = styled(CardHeader)`
  margin-left: 15;
  margin-right: 75;
`

export const CardFooterLeft = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 25%;
`

export const CardFooterRight = styled(CardFooterLeft)`
  justify-content: space-around;
`

export const SymbolIconButton = styled.TouchableOpacity``

export const SymbolIcon = styled.Image`
  width: 25;
  resizeMode: contain;
`

export const StyledOverlay = styled(Overlay)``

export const OverlayButton = styled.TouchableOpacity`
  justify-content: flex-start;
  align-items: flex-start;
  width: 60%;
  height: 50;
`

export const OverlayText = styled.Text`
  width: 100%;
  text-align: left;
  font-size: 16;
  color: black;
`
