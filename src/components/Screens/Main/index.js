import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import { Main } from './Main.js'


const selectors = createStructuredSelector({
})

const actions = {
}

export default connect(
  selectors,
  actions,
)(Main)
