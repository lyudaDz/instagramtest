import React, { Component } from 'react'
import { Share, View } from 'react-native'

import Overlay from 'react-native-modal-overlay'

import { UsersIconsHeader } from 'app/src/components/Blocks'

import { defaultUsersPhotos } from './config'
import * as Images from 'app/src/assets'

import {
  Container,
  ScrollPosts,
  FlatListPosts,
  Card,
  CardHeader,
  CardImage,
  UserAvatarWrapper,
  UserAvatarText,
  HeaderImageWrapper,
  HeaderImage,
  CardHeaderButton,
  CardHeaderButtonCicrcle,
  CardFooter,
  CardFooterLeft,
  CardFooterRight,
  SymbolIconButton,
  SymbolIcon,
  OverlayButton,
  OverlayText,
} from './styles'

class Main extends Component {
  state = {
    indexGlobal: -1,
    isVisibleModal: false,
  }

  componentDidMount() {
    const tempArr = [...defaultUsersPhotos]
    tempArr.forEach(item => (item.selected = false))
  }

  selectButtons = (item, index) => {
    const { indexGlobal } = this.state
    const tempArr = [...defaultUsersPhotos]
    if (indexGlobal !== index) {
      tempArr.forEach(item => (item.selected = false))
    }

    tempArr[index].selected = !tempArr[index].selected

    if (indexGlobal === index) {
      this.setState({
        indexGlobal: -1,
      })
    } else {
      this.setState({
        indexGlobal: index,
      })
    }
  }

  handleLike = () => {
    const { isLike } = this.state
    this.setState({ isLike: !isLike })
  }

  handleMessage = () => {}

  handleShare = async () => {
    try {
      const result = await Share.share({
        message: 'Hello from instagram',
      })

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
        } else {
        }
      } else if (result.action === Share.dismissedAction) {
      }
    } catch (error) {}
  }

  handleFav = () => {
    const { isFav } = this.state
    this.setState({ isFav: !isFav })
  }

  handleShowModal = () => this.setState({ isVisibleModal: true })

  handleCloseModal = () => this.setState({ isVisibleModal: false })

  renderListItem = ({ item, index }) => {
    const { indexGlobal, isLike, isFav } = this.state
    return (
      <Card key={item.id || index}>
        <CardHeader>
          <UserAvatarWrapper
            onPress={() => this.selectButtons(item, index)}
            border={
              (indexGlobal !== -1 && indexGlobal === index) || item.selected
            }
          >
            <HeaderImageWrapper>
              <HeaderImage source={Images[item.imgSrc]} />
            </HeaderImageWrapper>
            <UserAvatarText>{item.imgSrc}</UserAvatarText>
          </UserAvatarWrapper>
          <CardHeaderButton onPress={this.handleShowModal}>
            <CardHeaderButtonCicrcle />
            <CardHeaderButtonCicrcle />
            <CardHeaderButtonCicrcle />
          </CardHeaderButton>
        </CardHeader>
        <CardImage source={Images[item.imgSrc]} />
        <CardFooter>
          <CardFooterLeft>
            <SymbolIconButton onPress={this.handleLike}>
              <SymbolIcon
                source={
                  isLike ? Images['FavouriteActive'] : Images['Favourite']
                }
              />
            </SymbolIconButton>
            <SymbolIconButton onPress={this.handleMessage}>
              <SymbolIcon source={Images['Bubble']} />
            </SymbolIconButton>
            <SymbolIconButton onPress={this.handleShare}>
              <SymbolIcon source={Images['Direct']} />
            </SymbolIconButton>
          </CardFooterLeft>
          <CardFooterRight>
            <SymbolIconButton onPress={this.handleFav}>
              <SymbolIcon
                source={isFav ? Images['FavActive'] : Images['Fav']}
              />
            </SymbolIconButton>
          </CardFooterRight>
        </CardFooter>
      </Card>
    )
  }

  render() {
    const { isVisibleModal } = this.state
    return (
      <Container>
        <Overlay
          visible={isVisibleModal}
          onClose={this.handleCloseModal}
          closeOnTouchOutside
          animationType="zoomIn"
          containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }}
          childrenWrapperStyle={{
            alignItems: 'flex-start',
            height: 290,
            padding: 10,
            paddingLeft: 15,
            backgroundColor: '#eee',
            borderRadius: 5,
            overflow: 'hidden',
          }}
          animationDuration={100}
        >
          <OverlayButton>
            <OverlayText>Complain...</OverlayText>
          </OverlayButton>
          <OverlayButton>
            <OverlayText>Copy link</OverlayText>
          </OverlayButton>
          <OverlayButton>
            <OverlayText>Enable publication notifications</OverlayText>
          </OverlayButton>
          <OverlayButton>
            <OverlayText>Share link...</OverlayText>
          </OverlayButton>
          <OverlayButton>
            <OverlayText>Unsubscribe</OverlayText>
          </OverlayButton>
          <OverlayButton>
            <OverlayText>Switch to silent mode</OverlayText>
          </OverlayButton>
        </Overlay>
        <UsersIconsHeader />
        <ScrollPosts
          showsVerticalScrollIndicator={false}
        >
          <FlatListPosts
            renderItem={this.renderListItem}
            data={defaultUsersPhotos}
            keyEstractor={item => item.id}
          />
        </ScrollPosts>
      </Container>
    )
  }
}

export { Main }
