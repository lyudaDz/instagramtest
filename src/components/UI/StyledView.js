import styled from 'styled-components'

export const StyledView = styled.View`
  width: 100%;
  height: 100;
  flex-direction: row;
  justify-content: space-between;
`
