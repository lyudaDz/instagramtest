export const defaultUsersPhotos = [
  {
    id: 1,
    imgSrc: "First",
    selected: false,
    title: "First",
    type: "default"
  },
  {
    id: 2,
    imgSrc: "Second",
    selected: false,
    title: "Second"
  },
  {
    id: 3,
    imgSrc: "Third",
    selected: false,
    title: "Third"
  },
  {
    id: 4,
    imgSrc: "Fourth",
    selected: false,
    title: "Fourth"
  },
  {
    id: 5,
    imgSrc: "Fifth",
    selected: false,
    title: "Fifth"
  },
  {
    id: 6,
    imgSrc: "Sixth",
    selected: false,
    title: "Sixth"
  },
  {
    id: 7,
    imgSrc: "Seventh",
    selected: false,
    title: "Seventh"
  }
];
