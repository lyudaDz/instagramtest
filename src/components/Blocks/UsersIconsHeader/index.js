import React, { Component } from 'react'
import { View } from 'react-native'

import { isEmpty } from 'lodash'
import AsyncStorage from '@react-native-community/async-storage'
import ImagePicker from 'react-native-image-picker'

import { defaultUsersPhotos } from './config'
import * as Images from 'app/src/assets'

import {
  ScrollImages,
  StyledViewWrapper,
  UserPhotoWrapper,
  ImageWrapper,
  StyledRadialGradient,
  HeaderImageWrapper,
  HeaderImage,
  TextTitle,
  HorizontalFlatList,
  PlusButton,
  PlusButtonText,
} from './styles'

class UsersIconsHeader extends Component {
  state = {
    indexGlobal: -1,
    imageArr: null,
  }

  componentDidMount() {
    const tempArr = [...defaultUsersPhotos]
    tempArr.forEach(item => (item.selected = false))
    this.setState({ imageArr: [...defaultUsersPhotos] })
    // can`t use Async Storage too small limit - 6MB
    // this.initData()
  }

  initData = async () => {
    const { imageArr } = this.state
    try {
      if (isEmpty(imageArr)) {
        const imagesAsync = await AsyncStorage.getItem('imageArrTesterAAA')
        let imagesInitData = JSON.parse(imagesAsync)
        if (!isEmpty(imagesInitData)) {
          this.setState({
            imageArr: [defaultUsersPhotos[0], ...imagesInitData, ...defaultUsersPhotos.slice(1)],
          })
        } else {
          this.setState({ imageArr: [...defaultUsersPhotos] })
        }
      }
    } catch (err) {}
  }

  selectButtons = (item, index) => {
    const { indexGlobal } = this.state
    const tempArr = [...defaultUsersPhotos]
    if (indexGlobal !== index) {
      tempArr.forEach(item => (item.selected = false))
    }

    tempArr[index].selected = !tempArr[index].selected

    if (indexGlobal === index) {
      this.setState({
        indexGlobal: -1,
      })
    } else {
      this.setState({
        indexGlobal: index,
      })
    }
  }

  showCamera = () => {
    const { imageArr } = this.state

    const options = {
      title: 'Select Avatar',
      customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    }

    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
      } else {
        let source = response

        let sourceData = {
          id: imageArr.length + 1,
          imgSrc: source.data,
          selected: false,
          title: 'Photo',
        }

        // this.setAcyncStorage(sourceData)

        this.setState({
          imageArr: [...imageArr.slice(0, 1), sourceData, ...imageArr.slice(1)],
        })
      }
    })
  }

  setAcyncStorage = async sourceData => {
    const { imageArr } = this.state
    try {
      var imagesAsync = await AsyncStorage.getItem('imageArrTesterAAA')
      if (imagesAsync === null) {
        await AsyncStorage.setItem(
          'imageArrTesterAAA',
          JSON.stringify([sourceData]),
        )
      } else {
        let imagesInitData = JSON.parse(imagesAsync)
        finalInitData = [sourceData, ...imagesInitData]
        await AsyncStorage.setItem(
          'imageArrTesterAAA',
          JSON.stringify(finalInitData),
        )
      }
    } catch (err) {
    }
  }

  renderListItem = ({ item, index }) => {
    const { indexGlobal } = this.state
    return (
      <UserPhotoWrapper key={item.id || index}>
        <ImageWrapper
          isSizeBig={item.type === 'default'}
          onPress={() => this.selectButtons(item, index)}
          border={
            (indexGlobal !== -1 && indexGlobal === index) || item.selected
          }
        >
          {item.type !== 'default' && (
            <StyledRadialGradient
              colors={['#c20884', '#fed3a2']}
              stops={[0, 1]}
              center={[65, 0]}
              radius={100}
            />
          )}
          <HeaderImageWrapper
            isSizeBig={item.type === 'default'}
            onPress={this.showCamera}
            disabled={item.type !== 'default'}
          >
            {item.id > defaultUsersPhotos.length ? (
              <HeaderImage
                source={{
                  uri: 'data:image/jpeg;base64,' + item.imgSrc,
                }}
                isSizeBig={item.type === 'default'}
              />
            ) : (
              <HeaderImage
                source={Images[item.imgSrc]}
                isSizeBig={item.type === 'default'}
              />
            )}
          </HeaderImageWrapper>
          {item.type === 'default' && (
            <PlusButton>
              <PlusButtonText>+</PlusButtonText>
            </PlusButton>
          )}
        </ImageWrapper>
        <TextTitle numberOfLines={1}>{item.title}</TextTitle>
      </UserPhotoWrapper>
    )
  }

  render() {
    const { imageArr } = this.state
    return (
      <StyledViewWrapper>
        <ScrollImages>
          <HorizontalFlatList
            horizontal
            renderItem={this.renderListItem}
            data={imageArr}
            keyEstractor={item => item.id}
            showsHorizontalScrollIndicator={false}
          />
        </ScrollImages>
      </StyledViewWrapper>
    )
  }
}

export { UsersIconsHeader }
