import styled from 'styled-components'
import RadialGradient from 'react-native-radial-gradient'
import { StyledView } from 'app/src/components/UI/'

export const StyledViewWrapper = styled(StyledView)`
  align-items: center;
  margin-bottom: 5;
  background: white;
  z-index: 100;
`

export const ScrollImages = styled.ScrollView`
  height: 100;
`

export const UserPhotoWrapper = styled.View`
  justify-content: space-around;
  align-items: center;
  height: 100;
`

export const ImageWrapper = styled.TouchableOpacity`
  position: relative;
  justify-content: space-around;
  align-items: center;
  width: ${props => (props.isSizeBig ? 75 : 65)};
  height: ${props => (props.isSizeBig ? 75 : 65)};
  margin-right: 15;
  border-radius: 50;
  overflow: hidden;
`

export const StyledRadialGradient = styled(RadialGradient)`
  justify-content: space-around;
  align-items: center;
  width: 65;
  height: 65;
  border-radius: 50;
`

export const HeaderImageWrapper = styled.TouchableOpacity`
  position: absolute;
  top: 2.5;
  bottom: 2.5;
  left: 2.5;
  right: 2.5;
  width: ${props => (props.isSizeBig ? 65 : 60)};
  height: ${props => (props.isSizeBig ? 65 : 60)};
  margin-bottom: 15;
  padding: 5px;
  justify-content: space-around;
  align-items: center;
  border: 3px solid white;
  border-radius: 50;
  overflow: hidden;
`

export const HeaderImage = styled.Image`
  width: ${props => (props.isSizeBig ? 65 : 60)};
  height: ${props => (props.isSizeBig ? 65 : 60)};
  margin-bottom: 1;
`

export const TextTitle = styled.Text`
  width: 100%;
  margin-right: 15;
  font-size: 10;
  text-align: center;
  color: black;
`

export const HorizontalFlatList = styled.FlatList`
  padding-left: 5;
`

export const PlusButton = styled.View`
  position: absolute;
  bottom: 11;
  right: 8;
  justify-content: space-around;
  align-items: center;
  width: 15;
  height: 15;
  background: #3897F0;
  border: 1px solid white;
  border-radius: 50;
`

export const PlusButtonText = styled.Text`
  padding-bottom: 2;
  color: white;
`
