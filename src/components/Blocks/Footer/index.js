import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import Footer from './Footer.js'

const selectors = createStructuredSelector({

})

const actions = {

}

export default connect(
  selectors,
  actions,
)(Footer)
