import styled from "styled-components";

export const Container = styled.View`
  flex-direction: row;
  width: 100%;
  height: 50;
  background: #fafafa;
  border-width: 1;
  border-radius: 2;
  border-color: #ddd;
  border-bottom-width: 0;
`;

export const NavItem = styled.TouchableOpacity`
  width: 20%;
  height: 100%;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  background: #fafafa;
`;

export const HeaderImage = styled.Image`
  width: 25;
  height: 25;
  resizeMode: contain;
`;
