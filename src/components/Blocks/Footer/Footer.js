import React, { Component } from 'react'
import * as Images from 'app/src/assets/'

import { Container, NavItem, HeaderImage } from './styles'

export default class Footer extends Component {
  state = { active: 0 }

  componentDidUpdate(prevProps) {
    const { navigation } = this.props
    if (prevProps.navigation.state.index !== navigation.state.index) {
      if (navigation.state.index < 4) {
        this.setState({ active: navigation.state.index })
      } else {
        this.setState({ active: 4 })
      }
    }
  }

  toggleButtons = (name, index) => {
    const { navigation } = this.props
    navigation.navigate(name)
  }

  render() {
    const { active } = this.state
    return (
      <Container>
        <NavItem onPress={() => this.toggleButtons('Main', 0)}>
          <HeaderImage
            source={active === 0 ? Images['MainActive'] : Images['Main']}
          />
        </NavItem>
        <NavItem onPress={() => this.toggleButtons('Search', 1)}>
          <HeaderImage
            source={active === 1 ? Images['SearchActive'] : Images['Search']}
          />
        </NavItem>
        <NavItem onPress={() => this.toggleButtons('AddPhoto', 2)}>
          <HeaderImage source={Images['AddPhoto']} />
        </NavItem>
        <NavItem onPress={() => this.toggleButtons('Favourite', 3)}>
          <HeaderImage
            source={
              active === 3 ? Images['FavouriteActive'] : Images['Favourite']
            }
          />
        </NavItem>
        <NavItem onPress={() => this.toggleButtons('User', 4)}>
          <HeaderImage
            source={active === 4 ? Images['UserActive'] : Images['User']}
          />
        </NavItem>
      </Container>
    )
  }
}
